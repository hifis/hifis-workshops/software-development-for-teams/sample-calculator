<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->

# We Welcome your Contribution!

## General Options

- Please open **an issue**, if you have improvement ideas.
- Please open **a merge request**, if you want to share your improvements.

## Providing a Merge Request

- Please note that this **is not** a usual Git repository:
  - It demonstrates every important steps performed in the workshop.
  - Every branch shows the final results of a specific step.
  - The `default` branch adds further documentation.
- If you provide a merge request:
  - Create the merge request against the branch which initially introduces the content that you want to change.
  - Then, we will take over and rebase all branches building on it.
  - Finally, we have to make sure that the example is still in line with the workshop materials

**Example - Add change in `02a-initial-design`:**

```
git switch 02b<tab> && git rebase 02a<tab> && git push -f
git switch 03a<tab> && git rebase 02b<tab> && git push -f
git switch 03b<tab> && git rebase 03a<tab> && git push -f
git switch master<tab> && git rebase 03b<tab> && git push -f
```

> In case of merge conflicts, make sure to **skip** redundant changes.
