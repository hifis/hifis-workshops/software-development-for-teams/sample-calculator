# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
This package provides access to the different input methods. An input method
describes from which data source the sample values are retrieved.

.. note::
 * Use the function :func:`base.create_inputmethod` to create a concrete input method.

"""


# Restrict the public interface
from sample_calculator.inputs.base import InputMethodError, create_inputmethod
from sample_calculator.inputs.inputs.console_input import CONSOLE_INPUT
from sample_calculator.inputs.inputs.file_input import FILE_INPUT
